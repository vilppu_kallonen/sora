#-------------------------------------------------
#
# Project created by QtCreator 2016-05-04T11:07:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Sora
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        monttu.cpp

HEADERS  += mainwindow.h \
        monttu.h

FORMS    += mainwindow.ui \
        monttu.ui
