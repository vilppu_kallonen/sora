#ifndef MONTTU_H
#define MONTTU_H

#include <QDialog>
#include <string>

namespace Ui {
class Monttu;
}

class Monttu : public QDialog
{
    Q_OBJECT

public:
    explicit Monttu(QWidget *parent = 0);
    Monttu(int id); //param. rakentaja
    ~Monttu();  // purkaja

    // setter
    void setID(int id);

private slots:
    void on_pushButton_clicked();   // käsittelee napinpainalluksen

    void on_frame_objectNameChanged(const QString &objectName);

private:
    Ui::Monttu *ui;
    int id_=0;  // napin id
};

#endif // MONTTU_H
