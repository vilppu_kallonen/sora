#include "monttu.h"
#include "ui_monttu.h"

Monttu::Monttu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Monttu)
{
    ui->setupUi(this);
}

// param. rakentaja
Monttu::Monttu(int id): id_(id)
{

}

// purkaja
Monttu::~Monttu()
{
    delete ui;
}

// setter
void Monttu::setID(int id)
{
    id_ = id;

    // muuttaa labelin tekstiä id:n (painetun napin) mukaan
    if (id_ == 1)
    {
      ui->label->setText("Kivi 1");
    }
    else if (id_ == 2){
        ui->label->setText("Kivi 2");
    }
    else if (id_ == 3){
        ui->label->setText("Kivi 3");
    }
    else if (id_ == 4){
        ui->label->setText("Kivi 4");
    }

}

// sulkee ikkunan nappia painettaessa
void Monttu::on_pushButton_clicked()
{
    this->close();


}
