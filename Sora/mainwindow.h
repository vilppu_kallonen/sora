#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "monttu.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    // napinpainallukset
    void on_ButtonKivi1_clicked();

    void on_ButtonKivi2_clicked();

    void on_ButtonKivi3_clicked();

    void on_ButtonKivi4_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
