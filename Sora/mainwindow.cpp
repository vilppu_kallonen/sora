#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_ButtonKivi1_clicked()
{
    Monttu uusiMonttu;      // luodaan monttuolio
    uusiMonttu.setID(1);    // annetaan napille yksilöllinen id
    uusiMonttu.exec();      // suoritetaan monttuolio

}

void MainWindow::on_ButtonKivi2_clicked()
{
    Monttu uusiMonttu;
    uusiMonttu.setID(2);    // annetaan napille yksilöllinen id
    uusiMonttu.exec();
}

void MainWindow::on_ButtonKivi3_clicked()
{
    Monttu uusiMonttu;
    uusiMonttu.setID(3);    // annetaan napille yksilöllinen id
    uusiMonttu.exec();
}

void MainWindow::on_ButtonKivi4_clicked()
{
    Monttu uusiMonttu;
    uusiMonttu.setID(4);    // annetaan napille yksilöllinen id
    uusiMonttu.exec();
}
