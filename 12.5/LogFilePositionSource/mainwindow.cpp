#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
#include <QVector>
#include <QString>
#include <math.h>

#define PI 3.14159265

#include "logfilepositionsource.h"
#include "clientapplication.h"


mainWindow::mainWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::mainWindow)
{
    ui->setupUi(this);

    //Pixmap, kuva (mökki)
    QPixmap pix("C:/Users/Elmeri/Documents/QT/LogFilePositionSource/soramonttu.png");
    ui->lblY->setPixmap(pix);
    QPixmap pix3("C:/Users/Elmeri/Documents/QT/LogFilePositionSource/round.png");
    ui->ympura->setPixmap(pix3);
    // kuva, nuoli
    QPixmap pix2("C:/Users/Elmeri/Documents/QT/LogFilePositionSource/stick.png");
    ui->nuoli->setPixmap(pix2);



    // muuttuja, johon readNextPosition-funktiolta tuleva koordinaattijono tallennetaan
    QString str = "";
    // päivittyvä merkkijonomuuttuja
    QString tahantuleepaljontekstia = "";
    // luodaan LogFilePos-olio
    LogFilePositionSource test;
    //luodaan vektori koordinaateille
    QVector<double>koordinaatit;
    // aloitetaan loop joka käy läpi jokaisen rivin
     while (str != "0"){
        // luetaan olion readNextPosition-funktion palauttama koordinaatti muuttujaan
        str = test.readNextPosition();

        // tallennetaan kaikki tieto yhteen string-muuttujaan
        tahantuleepaljontekstia = tahantuleepaljontekstia + str;

        }
    // Parsitaan koordinaatit string taulukkoon
   QStringList koordinaatteja = tahantuleepaljontekstia.split(",");
   // poistetaan viimeinen nolla, jonka split-funktio luo
   if(koordinaatteja.at(koordinaatteja.size()-1)=="0"){
       koordinaatteja.removeLast();
   }
   // tuuttaa string-tablesta vectoriin, muuttaa jokaisebnngg koordinaatin doubleksi
   for(int i=0;i<koordinaatteja.size();i++)
        {
            koordinaatit.push_back(koordinaatteja.at(i).toDouble());
            std::cout<<koordinaatit.at(i)<<std::endl;   // printtaa konsoliin
        }
   //tulostetaan text Browseriin
   // otsikkorivi
   ui->textBrowser->setText("nro.\tLeveyaste \t Pituusaste");
   for(int i=0;i<koordinaatteja.size();i=i+2)
        {
            // j-muuttuja juoksevaa numerointia varten
           int j =i /2;
           // append lisää uudelle riville
           ui->textBrowser->append(QString::number(j+1)+".\t"+koordinaatteja.at(i)+" \t "+koordinaatteja.at(i+1));
        }
   double sorakasa1y = 100;             //lasketaan
   double sorakasa1x = 100;             //nuolelle
                                        //tuleva
   double y = sorakasa1y - 90;          //kulma
   double x = sorakasa1x - 180;          //
   double aste = 0;                     //muuttujan
                                        //arvot
   aste = atan(y/x);                    //vielä
   std::cout << aste << "\tkulma radiaaneissa"
   << std::endl;                        //mielivaltaisia
   aste = aste*(180/PI);                // <-- muuttaa radiaaneista asteiksi
   std::cout << aste << "\tkulma asteissa" <<  std::endl;

   // kun x on positiivinen
   if (x >= 0){
        aste = 90 - aste;


   }
   // kun x on negatiivinen
   else if (x < 0){
        aste = 270 - aste;
   }
   else {
       std::cout << "virhe!!!!!" << std::endl;
   }
   std::cout << aste << "\tnuolen oikea kulma" <<  std::endl;
   mainWindow::nuolenpyoritys(aste);
   x_ = aste+20;    // X_:lle asteen arvo, jotta nappi toimii :))):))
}

// purkaja
mainWindow::~mainWindow()
{
    delete ui;
}

// suorittaa koodin, kun nappia painetaan
void mainWindow::on_pushButton_clicked()
{
    // kuva
    QPixmap pix2("C:/Users/Elmeri/Documents/QT/LogFilePositionSource/stick.png");
    // kierto
    QMatrix rm;
    rm.rotate(x_); // x_ astetta
    pix2 =pix2.transformed(rm);
    // pnäytetään pkuva
    ui->nuoli->setPixmap(pix2);
    // tulostetaan asteet
    ui->textBrowser->append((QString::number(x_)));

    // nollataan 360 jälkeen (tai lisätään 10, 360->10)
    if (x_ >= 360){
        x_ = 20;
    }
    // muuten asteet +10
    else{
        x_ = x_ + 20;
    }
}

/*double laskeKulma(double latitude, double longitude)
{
    double sorakasa1y = 100;
    double sorakasa1x = 100;

    double y = sorakasa1y - latitude;
    double x = sorakasa1x - longitude;
    double aste = 0;

    //aste = atan(y/x)*(PI/180);
    std::cout << aste << std::endl;

    return aste;
}
*/

void mainWindow::nuolenpyoritys(double aste){
    // kuva
    QPixmap pix2("C:/Users/Elmeri/Documents/QT/LogFilePositionSource/stick.png");
    // kierto
    QMatrix rm;
    rm.rotate(aste); // x_ astetta
    pix2 =pix2.transformed(rm);
    // pnäytetään pkuva
    ui->nuoli->setPixmap(pix2);
    // tulostetaan asteet
    ui->textBrowser->append((QString::number(aste)));
}
