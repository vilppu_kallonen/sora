#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDialog>

namespace Ui {
class mainWindow;
}

class QGeoPositionInfo;
class mainWindow : public QDialog
{
    Q_OBJECT

public:
    explicit mainWindow(QWidget *parent = 0);
    ~mainWindow();
    //double laskeKulma(double latitude, double longitude);
    void nuolenpyoritys(double aste);

private slots:
    void on_pushButton_clicked();


private:
    Ui::mainWindow *ui;

    double x_ = 20.0; // tallennetaan kiertoasteet
    double latitude_ = 0.0;
    double longitude_ = 0.0;
};

#endif // MAINWINDOW_H
