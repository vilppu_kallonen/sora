#include <QApplication>

#include "clientapplication.h"
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    /*ClientApplication client;
    client.show();
    */
    mainWindow w;
    w.show();

    return app.exec();
}
