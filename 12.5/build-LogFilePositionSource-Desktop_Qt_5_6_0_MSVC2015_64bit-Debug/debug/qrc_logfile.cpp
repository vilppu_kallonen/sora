/****************************************************************************
** Resource object code
**
** Created by: The Resource Compiler for Qt version 5.6.0
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

static const unsigned char qt_resource_data[] = {
  // C:/Users/Elmeri/Documents/QT/LogFilePositionSource/simplelog.txt
  0x0,0x0,0x0,0x81,
  0x32,
  0x30,0x30,0x39,0x2d,0x30,0x38,0x2d,0x32,0x34,0x54,0x32,0x32,0x3a,0x32,0x35,0x3a,
  0x30,0x31,0x20,0x2d,0x32,0x37,0x2e,0x35,0x37,0x36,0x30,0x38,0x32,0x20,0x31,0x35,
  0x33,0x2e,0x30,0x39,0x32,0x34,0x31,0x35,0xd,0xa,0x32,0x30,0x30,0x39,0x2d,0x30,
  0x38,0x2d,0x32,0x34,0x54,0x32,0x32,0x3a,0x32,0x35,0x3a,0x30,0x32,0x20,0x2d,0x32,
  0x37,0x2e,0x35,0x37,0x36,0x32,0x32,0x33,0x20,0x31,0x35,0x33,0x2e,0x30,0x39,0x32,
  0x35,0x33,0x30,0xd,0xa,0x32,0x30,0x30,0x39,0x2d,0x30,0x38,0x2d,0x32,0x34,0x54,
  0x32,0x32,0x3a,0x32,0x35,0x3a,0x30,0x33,0x20,0x2d,0x32,0x37,0x2e,0x35,0x37,0x36,
  0x33,0x36,0x34,0x20,0x31,0x35,0x33,0x2e,0x30,0x39,0x32,0x36,0x34,0x38,0xd,0xa,
  
  
};

static const unsigned char qt_resource_name[] = {
  // simplelog.txt
  0x0,0xd,
  0x7,0x52,0xbf,0x14,
  0x0,0x73,
  0x0,0x69,0x0,0x6d,0x0,0x70,0x0,0x6c,0x0,0x65,0x0,0x6c,0x0,0x6f,0x0,0x67,0x0,0x2e,0x0,0x74,0x0,0x78,0x0,0x74,
  
};

static const unsigned char qt_resource_struct[] = {
  // :
  0x0,0x0,0x0,0x0,0x0,0x2,0x0,0x0,0x0,0x1,0x0,0x0,0x0,0x1,
  // :/simplelog.txt
  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x1,0x0,0x0,0x0,0x0,

};

#ifdef QT_NAMESPACE
#  define QT_RCC_PREPEND_NAMESPACE(name) ::QT_NAMESPACE::name
#  define QT_RCC_MANGLE_NAMESPACE0(x) x
#  define QT_RCC_MANGLE_NAMESPACE1(a, b) a##_##b
#  define QT_RCC_MANGLE_NAMESPACE2(a, b) QT_RCC_MANGLE_NAMESPACE1(a,b)
#  define QT_RCC_MANGLE_NAMESPACE(name) QT_RCC_MANGLE_NAMESPACE2( \
        QT_RCC_MANGLE_NAMESPACE0(name), QT_RCC_MANGLE_NAMESPACE0(QT_NAMESPACE))
#else
#   define QT_RCC_PREPEND_NAMESPACE(name) name
#   define QT_RCC_MANGLE_NAMESPACE(name) name
#endif

#ifdef QT_NAMESPACE
namespace QT_NAMESPACE {
#endif

bool qRegisterResourceData(int, const unsigned char *, const unsigned char *, const unsigned char *);

bool qUnregisterResourceData(int, const unsigned char *, const unsigned char *, const unsigned char *);

#ifdef QT_NAMESPACE
}
#endif

int QT_RCC_MANGLE_NAMESPACE(qInitResources_logfile)();
int QT_RCC_MANGLE_NAMESPACE(qInitResources_logfile)()
{
    QT_RCC_PREPEND_NAMESPACE(qRegisterResourceData)
        (0x01, qt_resource_struct, qt_resource_name, qt_resource_data);
    return 1;
}

int QT_RCC_MANGLE_NAMESPACE(qCleanupResources_logfile)();
int QT_RCC_MANGLE_NAMESPACE(qCleanupResources_logfile)()
{
    QT_RCC_PREPEND_NAMESPACE(qUnregisterResourceData)
       (0x01, qt_resource_struct, qt_resource_name, qt_resource_data);
    return 1;
}

namespace {
   struct initializer {
       initializer() { QT_RCC_MANGLE_NAMESPACE(qInitResources_logfile)(); }
       ~initializer() { QT_RCC_MANGLE_NAMESPACE(qCleanupResources_logfile)(); }
   } dummy;
}
