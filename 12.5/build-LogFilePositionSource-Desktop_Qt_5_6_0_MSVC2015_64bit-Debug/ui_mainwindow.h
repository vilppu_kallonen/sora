/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>

QT_BEGIN_NAMESPACE

class Ui_mainWindow
{
public:
    QTextBrowser *textBrowser;
    QLabel *lblY;
    QPushButton *pushButton;
    QLabel *nuoli;
    QLabel *ympura;

    void setupUi(QDialog *mainWindow)
    {
        if (mainWindow->objectName().isEmpty())
            mainWindow->setObjectName(QStringLiteral("mainWindow"));
        mainWindow->resize(635, 382);
        mainWindow->setMinimumSize(QSize(635, 382));
        mainWindow->setMaximumSize(QSize(635, 382));
        textBrowser = new QTextBrowser(mainWindow);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setGeometry(QRect(9, 9, 256, 361));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(textBrowser->sizePolicy().hasHeightForWidth());
        textBrowser->setSizePolicy(sizePolicy);
        lblY = new QLabel(mainWindow);
        lblY->setObjectName(QStringLiteral("lblY"));
        lblY->setGeometry(QRect(271, 9, 354, 298));
        lblY->setPixmap(QPixmap(QString::fromUtf8("soramonttu.png")));
        pushButton = new QPushButton(mainWindow);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(271, 313, 354, 58));
        sizePolicy.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
        pushButton->setSizePolicy(sizePolicy);
        nuoli = new QLabel(mainWindow);
        nuoli->setObjectName(QStringLiteral("nuoli"));
        nuoli->setGeometry(QRect(393, 96, 101, 81));
        sizePolicy.setHeightForWidth(nuoli->sizePolicy().hasHeightForWidth());
        nuoli->setSizePolicy(sizePolicy);
        nuoli->setPixmap(QPixmap(QString::fromUtf8("stick.png")));
        nuoli->setScaledContents(true);
        ympura = new QLabel(mainWindow);
        ympura->setObjectName(QStringLiteral("ympura"));
        ympura->setGeometry(QRect(400, 100, 91, 81));
        ympura->setPixmap(QPixmap(QString::fromUtf8("round.png")));
        ympura->setScaledContents(true);
        lblY->raise();
        ympura->raise();
        textBrowser->raise();
        pushButton->raise();
        nuoli->raise();

        retranslateUi(mainWindow);

        QMetaObject::connectSlotsByName(mainWindow);
    } // setupUi

    void retranslateUi(QDialog *mainWindow)
    {
        mainWindow->setWindowTitle(QApplication::translate("mainWindow", "Soramonttu", 0));
        lblY->setText(QString());
        pushButton->setText(QApplication::translate("mainWindow", "PushButton", 0));
        nuoli->setText(QString());
        ympura->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class mainWindow: public Ui_mainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
