/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>

QT_BEGIN_NAMESPACE

class Ui_mainWindow
{
public:
    QTextBrowser *textBrowser;
    QPushButton *pushButton1;
    QPushButton *BtnKasa1;
    QPushButton *BtnKasa2;
    QPushButton *BtnKasa3;
    QPushButton *BtnKasa4;
    QPushButton *BtnKotiin;

    void setupUi(QDialog *mainWindow)
    {
        if (mainWindow->objectName().isEmpty())
            mainWindow->setObjectName(QStringLiteral("mainWindow"));
        mainWindow->resize(635, 382);
        mainWindow->setMinimumSize(QSize(635, 382));
        mainWindow->setMaximumSize(QSize(635, 382));
        textBrowser = new QTextBrowser(mainWindow);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setGeometry(QRect(0, 0, 261, 111));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(textBrowser->sizePolicy().hasHeightForWidth());
        textBrowser->setSizePolicy(sizePolicy);
        pushButton1 = new QPushButton(mainWindow);
        pushButton1->setObjectName(QStringLiteral("pushButton1"));
        pushButton1->setGeometry(QRect(404, 313, 221, 58));
        sizePolicy.setHeightForWidth(pushButton1->sizePolicy().hasHeightForWidth());
        pushButton1->setSizePolicy(sizePolicy);
        BtnKasa1 = new QPushButton(mainWindow);
        BtnKasa1->setObjectName(QStringLiteral("BtnKasa1"));
        BtnKasa1->setGeometry(QRect(10, 120, 241, 51));
        BtnKasa2 = new QPushButton(mainWindow);
        BtnKasa2->setObjectName(QStringLiteral("BtnKasa2"));
        BtnKasa2->setGeometry(QRect(10, 180, 241, 51));
        BtnKasa3 = new QPushButton(mainWindow);
        BtnKasa3->setObjectName(QStringLiteral("BtnKasa3"));
        BtnKasa3->setGeometry(QRect(10, 240, 241, 51));
        BtnKasa4 = new QPushButton(mainWindow);
        BtnKasa4->setObjectName(QStringLiteral("BtnKasa4"));
        BtnKasa4->setGeometry(QRect(10, 300, 241, 51));
        BtnKotiin = new QPushButton(mainWindow);
        BtnKotiin->setObjectName(QStringLiteral("BtnKotiin"));
        BtnKotiin->setGeometry(QRect(270, 314, 121, 53));

        retranslateUi(mainWindow);

        QMetaObject::connectSlotsByName(mainWindow);
    } // setupUi

    void retranslateUi(QDialog *mainWindow)
    {
        mainWindow->setWindowTitle(QApplication::translate("mainWindow", "Soramonttu", 0));
        pushButton1->setText(QApplication::translate("mainWindow", "Py\303\266rit\303\244", 0));
        BtnKasa1->setText(QApplication::translate("mainWindow", "Kasa 1", 0));
        BtnKasa2->setText(QApplication::translate("mainWindow", "Kasa 2", 0));
        BtnKasa3->setText(QApplication::translate("mainWindow", "Kasa 3", 0));
        BtnKasa4->setText(QApplication::translate("mainWindow", "Kasa 4", 0));
        BtnKotiin->setText(QApplication::translate("mainWindow", "Kotiin", 0));
    } // retranslateUi

};

namespace Ui {
    class mainWindow: public Ui_mainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
