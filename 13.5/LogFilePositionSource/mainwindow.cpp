#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
#include <QVector>
#include <QString>
#include <math.h>
#include <QtCore>
#include <QtGui>

#define PI 3.14159265

#include "logfilepositionsource.h"
#include "clientapplication.h"


mainWindow::mainWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::mainWindow)
{
    ui->setupUi(this);
/*
    //Pixmap, kuva (mökki)
    QPixmap pix("C:/Users/Elmeri/Documents/QT/LogFilePositionSource/soramonttu.png");
    ui->lblY->setPixmap(pix);
    QPixmap pix3("C:/Users/Elmeri/Documents/QT/LogFilePositionSource/round.png");
    ui->ympura->setPixmap(pix3);
    // kuva, nuoli
    QPixmap pix2("C:/Users/Elmeri/Documents/QT/LogFilePositionSource/stick.png");
    ui->nuoli->setPixmap(pix2);
*/


    // muuttuja, johon readNextPosition-funktiolta tuleva koordinaattijono tallennetaan
    QString str = "";
    // päivittyvä merkkijonomuuttuja
    QString tahantuleepaljontekstia = "";
    // luodaan LogFilePos-olio
    LogFilePositionSource test;

    // aloitetaan loop joka käy läpi jokaisen rivin
     while (str != "0"){
        // luetaan olion readNextPosition-funktion palauttama koordinaatti muuttujaan
        str = test.readNextPosition();

        // tallennetaan kaikki tieto yhteen string-muuttujaan
        tahantuleepaljontekstia = tahantuleepaljontekstia + str;

        }
    // Parsitaan koordinaatit string taulukkoon
   QStringList koordinaatteja = tahantuleepaljontekstia.split(",");
   // poistetaan viimeinen nolla, jonka split-funktio luo
   if(koordinaatteja.at(koordinaatteja.size()-1)=="0"){
       koordinaatteja.removeLast();
   }
   // tuuttaa string-tablesta vectoriin, muuttaa jokaisebnngg koordinaatin doubleksi
   for(int i=0;i<koordinaatteja.size();i++)
        {
            koordinaatit_.push_back(koordinaatteja.at(i).toDouble());
            std::cout<<koordinaatit_.at(i)<<std::endl;   // printtaa konsoliin
        }
   //tulostetaan text Browseriin
   // otsikkorivi
   ui->textBrowser->setText("nro.\tLeveyaste \t Pituusaste");
   for(int i=0;i<koordinaatteja.size();i=i+2)
        {
            // j-muuttuja juoksevaa numerointia varten
           int j =i /2;
           // append lisää uudelle riville
           ui->textBrowser->append(QString::number(j+1)+".\t"+koordinaatteja.at(i)+" \t "+koordinaatteja.at(i+1));
        }
}

// purkaja
mainWindow::~mainWindow()
{
    delete ui;
}


double mainWindow::laskeKulma(double latitude, double longitude)
{
    double sorakasa1y = 0;             //lasketaan
    double sorakasa1x = 0;             //nuolelle
                                         //tuleva
    double y = sorakasa1y - latitude;          //kulma
    double x = sorakasa1x - longitude;          //
    double aste = 0;                     //muuttujan
                                         //arvot
    aste = atan(y/x);                    //vielä
    std::cout << aste << "\tkulma radiaaneissa"
    << std::endl;                        //mielivaltaisia
    aste = aste*(180/PI);                // <-- muuttaa radiaaneista asteiksi
    std::cout << aste << "\tkulma asteissa" <<  std::endl;

    // kun x on positiivinen
    if (x >= 0){
         aste = 90 - aste;


    }
    // kun x on negatiivinen
    else if (x < 0){
         aste = 270 - aste;
    }
    else {
        std::cout << "virhe!!!!!" << std::endl;
    }
    std::cout << aste << "\tnuolen oikea kulma" <<  std::endl;
    return aste;
}

/*
void mainWindow::nuolenpyoritys(double aste){
    // kuva
    QPixmap pix2("C:/Users/Elmeri/Documents/QT/LogFilePositionSource/stick.png");
    // kierto
    QMatrix rm;
    rm.rotate(aste); // x_ astetta
    pix2 =pix2.transformed(rm);
    // pnäytetään pkuva
    ui->nuoli->setPixmap(pix2);
    // tulostetaan asteet
    ui->textBrowser->append((QString::number(aste)));
}
*/
void mainWindow::paintEvent(QPaintEvent *)
{
    int side = qMin(width(), height());

    QRectF target(260.0, 0.0, side+20,side+20);
    QRectF source(0.0, 0.0, side,side);
    QImage image("C:/Users/Elmeri/Documents/QT/LogFilePositionSource/soramonttu.png");

    QPainter paint(this);
    paint.drawImage(target, image, source);
    QPainter painter(this);

    static const QPoint nuoli[4] = {
            QPoint(30, 40),
            QPoint(0, 28),
            QPoint(-30, 40),
            QPoint(0, -70)
        };

    QColor nuoliColor(112, 128, 180, 191);



    painter.setRenderHint(QPainter::Antialiasing);
        painter.translate(width() / 1.4, height() / 2.2);
        painter.scale(side / 400.0, side / 400.0);

    painter.setPen(Qt::NoPen);
        painter.setBrush(nuoliColor);

        painter.save();
            painter.rotate(x_);
            painter.drawConvexPolygon(nuoli, 4);
            painter.restore();


            QPen pointpen(Qt::blue);
            pointpen.setWidth(1);
            painter.setPen(pointpen);
            painter.drawPoint(0,0);
}

void mainWindow::on_pushButton1_pressed()
{
    if (x_ >= 360){
        x_ = 10;
    }
    // muuten asteet +10
    else{
        x_ = x_ + 10;
    }
    update();

    ui->pushButton1->setAutoRepeat(true);



}

void mainWindow::on_BtnKasa1_clicked()
{
    double x = koordinaatit_.at(0);
    double y = koordinaatit_.at(1);
    x_= laskeKulma(y,x);
    update();
}

void mainWindow::on_BtnKasa2_clicked()
{
    double x = koordinaatit_.at(2);
    double y = koordinaatit_.at(3);
    x_= laskeKulma(y,x);
    update();
}

void mainWindow::on_BtnKasa3_clicked()
{
    double x = koordinaatit_.at(4);
    double y = koordinaatit_.at(5);
    x_= laskeKulma(y,x);
    update();
}

void mainWindow::on_BtnKasa4_clicked()
{
    double x = koordinaatit_.at(6);
    double y = koordinaatit_.at(7);
    x_= laskeKulma(y,x);
    update();
}

void mainWindow::on_BtnKotiin_clicked()
{
    double x = koordinaatit_.at(8);
    double y = koordinaatit_.at(9);
    x_= laskeKulma(y,x);
    update();
}
