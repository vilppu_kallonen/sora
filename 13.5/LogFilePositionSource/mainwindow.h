#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDialog>

namespace Ui {
class mainWindow;
}

class QGeoPositionInfo;
class mainWindow : public QDialog
{
    Q_OBJECT

public:
    explicit mainWindow(QWidget *parent = 0);
    ~mainWindow();
    double laskeKulma(double latitude, double longitude);


private slots:

    void on_pushButton1_pressed();
    void on_BtnKasa1_clicked();

    void on_BtnKasa2_clicked();

    void on_BtnKasa3_clicked();

    void on_BtnKasa4_clicked();

    void on_BtnKotiin_clicked();

protected:
    void paintEvent(QPaintEvent *);


private:
    Ui::mainWindow *ui;
    //luodaan vektori koordinaateille
    QVector<double>koordinaatit_;
    double x_ = 0.0; // tallennetaan kiertoasteet
    double latitude_ = 0.0;
    double longitude_ = 0.0;
};

#endif // MAINWINDOW_H
