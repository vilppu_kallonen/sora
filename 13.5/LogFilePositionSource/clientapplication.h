#ifndef CLIENTAPPLICATION_H
#define CLIENTAPPLICATION_H

#include <QMainWindow>

class QGeoPositionInfo;
class QTextEdit;

class ClientApplication : public QMainWindow
{
    Q_OBJECT
public:
    ClientApplication(QWidget *parent = 0);

private slots:
    void positionUpdated(const QGeoPositionInfo &info);

private:
    QTextEdit *textEdit;
};

#endif
